import './App.css';
import React, { useState, useEffect } from 'react'

import apiservice from './services/aircondition.js'
import Status from './components/Status';
import Daily from './components/Daily';
import History from './components/History';
//import MessageBox from './components/MessageBox';


const App = () => {
  document.title = "AirConditionMonitor"

  const [devices, setDevices] = useState([])
  //const [open, setOpen] = React.useState(false)
  //const [message, setMessage] = useState('')

  useEffect(() => {
    apiservice
      .getDevices()
      .then(data => {
        setDevices(data)
      })
      .catch(error => {
        //setOpen(true)
      })


  }, [])


  return (
    <div className="App">
      <header className="desktop-header">

        {/* TODO: make global with redux */}
    {/*<MessageBox level="error" text={message} open={open} setOpen={setOpen} />*/}

        <div>
          <h2>Daily</h2>
          <Daily devices={devices} />
        </div>

        <div>
          <h2>Status</h2>
          <Status />
        </div>

        <div>
          <h2>History</h2>
          <History devices={devices}/>
        </div>

      </header>
    </div>
  )

}

export default App
