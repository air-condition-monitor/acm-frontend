import axios from 'axios'

const baseUrl = "/api"


const getCurrent = (deviceLocation, device) => {
  const req = axios.get(`${baseUrl}/current/${deviceLocation}/${device}`)
  return req.then(res => res.data)
}


const getRange = (dateFrom, dateTo, device) => {
  const from = new Date(dateFrom.toString())
  const to = new Date(dateTo.toString())

  from.setTime(from.getTime() - from.getTimezoneOffset()*60*1000)
  to.setTime(to.getTime() - to.getTimezoneOffset()*60*1000)

  const strFrom = from.toISOString().split('.')[0].split('T')[0]
  const strTo = to.toISOString().split('.')[0].split('T')[0]

  const req = axios.get(`${baseUrl}/range?device=${device}&from=${strFrom}&to=${strTo}`)
  return req.then(res => res.data)
}


const getDaily = (device, date) => {
  const req = axios.get(`${baseUrl}/day?device=${device}&date=${date}`)
  return req.then(res => res.data)
}


const getDevices = () => {
  const req = axios.get(`${baseUrl}/devices`)
  return req.then(res => res.data)
}



const exported = { getCurrent, getRange, getDaily, getDevices }
export default exported;
