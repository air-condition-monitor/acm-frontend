import React from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';


const Alert = (props) => {
  return (
    <MuiAlert elevation={6} variant="filled" {...props} />
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}))


const MessageBox = ({level, text, open, setOpen}) => {
  const classes = useStyles()

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setOpen(false)
  }

  return (
    <div>
      <Snackbar open={open} autoHideDuration={5} onClose={handleClose}>

        <Alert onClose={handleClose} severity={level}>
          {text}
        </Alert>

      </Snackbar>
    </div>
  );
}


export default MessageBox
