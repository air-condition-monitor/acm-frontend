
const formatTitle = (tooltipItems) => {
  if (tooltipItems.length > 0) {
    const item = tooltipItems[0];
    const labels = item.chart.data.labels;
    const labelCount = labels ? labels.length : 0;

    if (this && this.options && this.options.mode === 'dataset') {
      return item.dataset.label || '';

    // as many labels as datapoints or item.label is defined
    } else if (item.label) {
      const dt = new Date(item.label)
      const options = {dateStyle: "short", timeStyle: "short"}

      return dt.toLocaleString("fi-FI", options).toString();

    // less labels than datapoints
    } else if (labelCount > 0 && item.dataIndex < labelCount) {
      return labels[item.dataIndex];
    }
  }

  return '';
}


const formatLabel = (tooltipItem) => {

  if (this && this.options && this.options.mode === 'dataset') {
    return tooltipItem.label + ': ' + tooltipItem.formattedValue || tooltipItem.formattedValue;
  }

  let label = tooltipItem.dataset.label || '';

  if (label) {
    label += ': ';
  }

  const value = tooltipItem.formattedValue;
  if (value && value != null) {
    label += value;
  }

  return label;
}


const Tooltip = {
    enabled: true,
    external: null,
    position: 'average',
    backgroundColor: 'rgba(0,0,0,0.8)',
    titleColor: '#fff',
    titleFont: { weight: 'bold', },
    titleSpacing: 2,
    titleMarginBottom: 6,
    titleAlign: 'left',
    bodyColor: '#fff',
    bodySpacing: 2,
    bodyFont: {},
    bodyAlign: 'left',
    footerColor: '#fff',
    footerSpacing: 2,
    footerMarginTop: 6,
    footerFont: { weight: 'bold', },
    footerAlign: 'left',
    padding: 6,
    caretPadding: 2,
    caretSize: 5,
    cornerRadius: 6,
    boxHeight: (ctx, opts) => opts.bodyFont.size,
    boxWidth: (ctx, opts) => opts.bodyFont.size,
    multiKeyBackground: '#fff',
    displayColors: true,
    borderColor: 'rgba(0,0,0,0)',
    borderWidth: 0,

    animation: {
      duration: 400,
      easing: 'easeOutQuart',
    },

    animations: {
      numbers: {
        type: 'number',
        properties: ['x', 'y', 'width', 'height', 'caretX', 'caretY'],
      },
      opacity: {
        easing: 'linear',
        duration: 200
      }
    },

    callbacks: {
      title: formatTitle,
      label: formatLabel,
    }

}



export default Tooltip;
