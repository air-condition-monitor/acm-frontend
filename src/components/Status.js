import React, { useState, useEffect } from 'react'
import { WiThermometer, WiHumidity } from "react-icons/wi";
import apiservice from '../services/aircondition.js'

const PADDING = "\u00a0".repeat(4)

const wrapperStyle = {
  display: 'block',
  marginTop: 'auto',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: `100%`,
  height: '100%',
}

const Status = () => {
  const [data, setData] = useState([])

  const DEVICE = "ruuvi"

  useEffect(() => {
    apiservice
      .getCurrent("inside", DEVICE)
      .then(data => {
        setData(data)
      })
      .catch(error => {
        // TODO: use snackbar https://material-ui.com/components/snackbars/
      })


  }, [])

  if (!data || data.length === 0) {
    return (
      <div>
        <p>No data for &quot;{DEVICE}&quot;</p>
      </div>
    )

  } else {
    const temp = data[0]["temperature"]
    const humid = data[0]["humidity"]
    const time = new Date(data[0]["time"])
    const device = data[0]["device"]

    const tempStr = String(temp.toFixed(1)) + "°C"
    const humidStr = humid ? String(humid.toFixed(1)) + "%" : "undef"
    const lastUpdated = time.toUTCString()
    const deviceStr = device[0].toUpperCase() + device.substr(1)

    return (
      <div style={wrapperStyle}>
        <p> <WiThermometer size="3rem"/>{deviceStr}:{PADDING}{tempStr} </p>
        <p> <WiHumidity size="3rem"/>{deviceStr}:{PADDING}{humidStr} </p>
        <p>Last Updated:<br></br>{lastUpdated}</p>
      </div>
    )
  }

}


export default Status
