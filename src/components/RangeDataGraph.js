import React, { useState, useEffect } from 'react'
import DataGraph from './DataGraph';
import apiservice from '../services/aircondition.js'


const RangeDataGraph = ({dateFrom, dateTo, device, datatypes}) => {
  const [data, setData] = useState([])

  // called when date range and device change
  useEffect(() => {

    if (device) {
      apiservice
        .getRange(dateFrom, dateTo, device)
        .then(data => {
          setData(data)
        })
        .catch(error => {
          // TODO: use snackbar https://material-ui.com/components/snackbars/
        })
    }


  }, [dateFrom, dateTo, device])

  // TODO: drop down menu for different datatypes, range for one at a time only?
  return (
    <div>
      <DataGraph data={data} device={device} datatypes={datatypes}/>
    </div>
  )

}

export default RangeDataGraph

