import React, { useState } from 'react'
import RangeDataGraph from './RangeDataGraph';
import DateRangePicker from './DateRangePicker';
import DevicePicker from './DevicePicker.js';


const types =
{
  "temperature":
  {
    "bg-color": "rgb(200, 50, 0)",
    "border-color": "rgba(255, 0, 0, 0.5)"
  },

  "humidity":
  {
    "bg-color": "rgb(80, 140, 255)",
    "border-color": "rgba(0, 0, 255, 0.5)"
  },

  "pressure":
  {
    "bg-color": "rgb(120, 255, 150)",
    "border-color": "rgba(0, 255, 0, 0.5)"
  },

}


const History = ({devices}) => {
  const today = new Date()
  const yesterday = new Date()
  yesterday.setDate(yesterday.getDate() - 1)

  const [dateFrom, setDateFrom] = useState(yesterday)
  const [dateTo, setDateTo] = useState(today)
  const [chosenDevice, setChosenDevice] = useState('')

  return (
    <div>
      <DateRangePicker
        setDateFrom={setDateFrom}
        dateFrom={dateFrom}
        setDateTo={setDateTo}
        dateTo={dateTo}
        upperLimit={today}
      />

      <DevicePicker
        className="devicepicker"
        devices={devices}
        chosen={chosenDevice}
        setChosen={setChosenDevice}
      />

      <RangeDataGraph dateFrom={dateFrom} dateTo={dateTo} device={chosenDevice} datatypes={types}/>
    </div>
  )
}


export default History
