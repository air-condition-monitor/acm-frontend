import React from 'react';

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  Legend,
} from 'recharts';

import { roundMod, floorTime, getTime } from './utility.js'


const wrapperStyle = {
  display: 'block',
  marginTop: 'auto',
  marginLeft: 'auto',
  marginRight: 'auto',
  width: `100%`,
  height: '100%',
}

const chartMargins = {
  top: 20,
  right: 20,
  left: 0,
  bottom: 20
}


const getRange = (types, type, defaultRange) => {
  const min = "min" in types[type] ? types[type]["min"] : defaultRange[0]
  const max = "max" in types[type] ? types[type]["max"] : defaultRange[1]

  return [min, max]
}


const getDefaultRange = (data, type) => {
  const min = Math.min.apply(null, data.map(p => p[type]))
  const max = Math.max.apply(null, data.map(p => p[type]))

  return [roundMod(min, 5) - 5, roundMod(max, 5) + 5]
}


// TODO: more than 2 datatypes (3 breaks the chart)
const Rechart = ({data, datatypes, devices, title}) => {

  return (
    <div style={wrapperStyle}>
      <LineChart margins={chartMargins} width={400} height={300}>

        <CartesianGrid strokeDasharray="5 1" horizontal={true} vertical={true} />

        <XAxis
          dataKey="time"
          tickCount={10}
          tickMargin={10}
          tick={{fontSize: 15}}
          /* TODO: configurable for Daily/History */
          tickFormatter={(iso_time) => floorTime(getTime(iso_time)) }
        />

        {
          Object.keys(datatypes).map((type, index) => {
            return (
              <YAxis
                key={`key-axis-${type}`}
                dataKey={type}
                type="number"
                domain={getRange(
                  datatypes,
                  type,
                  getDefaultRange(data, type)
                )}
                interval={0}
                tick={{fontSize: 20}}
                allowDataOverflow={false}
                orientation={index % 2 === 0 ? "left" : "right"}
                yAxisId={`y-axis-${type}`}
              />
            )
          })
        }

        <Tooltip
          wrapperStyle={{
            borderColor: 'white',
            boxShadow: '2px 2px 3px 0px rgb(204, 204, 204)',
          }}
          contentStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.8)' }}
          labelStyle={{ fontWeight: 'bold', color: '#666666' }}
        />

        <Legend verticalAlign="top" height={36}/>
        {
          Object.keys(datatypes).map(type => {
            return (
              <Line
                key={`key-data-${type}`}
                data={data}
                name={type}
                dataKey={type}
                stroke={datatypes[type]["bg-color"]}
                type="monotone"
                dot={false}
                yAxisId={`y-axis-${type}`}
              />
            )
          })
        }

      </LineChart>
    </div>
  )


}


export default Rechart
