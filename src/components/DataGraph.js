import React from 'react';
import  ReactChart from './ReactChart.js'
import Rechart from './Rechart.js'


const DataGraph = ({data, device, datatypes, title}) => {

  if (!device) {
    return (
      <div>
        <p>No device</p>
      </div>
    )
  }

  if (data.length === 0) {
    return (
      <div>
        <p>No data</p>
      </div>
    )
  }

  //data.sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime())

  // TODO: multi device support
  // new DataGraph for each device?
  // TODO: new graph component for comparing any device and any datatype
  const filteredData = data.filter(point => point["device"] === device)

  return (
    <div>
      <ReactChart
        data={filteredData}
        datatypes={datatypes}
        title={title}
      />
    </div>
  )

}

export default DataGraph

