import React, { useState, useEffect } from 'react'
import { getToday } from './utility.js'
import DataGraph from './DataGraph';
import apiservice from '../services/aircondition.js'


const types =
{
  "temperature":
  {
    "bg-color": "rgb(200, 50, 0)",
    "border-color": "rgba(255, 0, 0, 0.5)",
    "min": 20,
    "max": 30,
  },

  "humidity":
  {
    "bg-color": "rgb(80, 140, 255)",
    "border-color": "rgba(0, 0, 255, 0.5)",
    "min": 40,
    "max": 100,
  },
  /*

  "pressure":
  {
    "bg-color": "rgb(120, 255, 150)",
    "border-color": "rgba(0, 255, 0, 0.5)"
  },
  */

}


const Daily = ({devices}) => {
  const [data, setData] = useState([])

  useEffect(() => {
    apiservice
      //.getDaily("any", getToday())
      .getDaily("any", "2021-08-18")
      .then(data => {
        setData(data)
      })
      .catch(error => {
        // TODO: use snackbar https://material-ui.com/components/snackbars/
      })


  }, []);

  return (
    <div>
      <DataGraph data={data} device={devices[0]} datatypes={types} title={devices[0]}/>
    </div>
  )
}



export default Daily
