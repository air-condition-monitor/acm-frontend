import React from 'react';
import { Line } from 'react-chartjs-2'
import { floorTime, getTime } from './utility.js'
import Tooltip from './Tooltip.js'


const ReactChart = ({data, datatypes, devices, title}) => {

  const times = data.map(p => floorTime( p["time"] ) )

  const dataset = {
    labels: times,

    // TODO: generate points for different devices (current datatype mapping
    // doesn't allow same type with diff dev)
    datasets: Object.keys(datatypes).map(type => {

      const points = data.map(p => p[type])

      return {
        label: type,
        data: points,
        fill: false,
        position: 'right',
        backgroundColor: datatypes[type]["bg-color"],
        borderColor: datatypes[type]["border-color"],
        yAxisID: `y-axis-${type}`,
      }

    })
  }

  const options = {
    scales: {
      x: {

        ticks: {
          // For a category axis, the val is the index so the lookup via
          // getLabelForValue is needed

          // TODO: show always n ticks, charjs's automatic good enough?
          callback: function(val, index) {
            const label = getTime(this.getLabelForValue(val))

            return label;
            /*
            if (data.length > TICK_COUNT) {
              return index % Math.floor(data.length / TICK_COUNT) === 0 ? label : '';
            } else {
              return label;
            }
            */

          },
        }


      }

    },

    plugins: {
      tooltip: Tooltip,
    }

  }

  let i = 0
  for (const type in datatypes) {

    options["scales"][`y-axis-${type}`] = {
      position: i % 2 === 0 ? "left" : "right",
      type: "linear",
      ticks: {
        beginAtZero: false,
      }
    }

    ++i
  }


  return (
    <div>
      <h5>{title}</h5>
      <Line
        data={dataset}
        options={options}
        width={400}
        height={300}
      />
    </div>
  )

}

export default ReactChart
