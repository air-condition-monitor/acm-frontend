import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Box from "@material-ui/core/Box";

const DateRangePicker = ({setDateFrom, dateFrom, setDateTo, dateTo, upperLimit}) => {
  /* TODO: side by side if screen width allows -----------------------------*/
  return (
    <div>
      <Box>
        <h3 >From</h3>
        <DatePicker
          dateFormat="dd.MM.yyyy"
          onChange={setDateFrom}
          selected={dateFrom}
          maxDate={dateTo}
        />
      </Box>

      <Box>
        <h3 >To</h3>
        <DatePicker
          dateFormat="dd.MM.yyyy"
          onChange={setDateTo}
          selected={dateTo}
          minDate={dateFrom}
          maxDate={upperLimit}
        />
      </Box>
    </div>
  )
  /* -----------------------------------------------------------------------*/
}


export default DateRangePicker
