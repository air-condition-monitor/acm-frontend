
const getToday = () => {
  const today = new Date()
  today.setTime(today.getTime() - today.getTimezoneOffset()*60*1000)
  let datestr = String(today.toISOString())
  datestr = datestr.split("T")[0]

  return datestr
}


const getTime = (datestr) => {

  if (!datestr) {
    return 0
  }

  const time = datestr.split("T")[1]
  const index = time.indexOf("Z")
  return index !== -1 ? time.substr(0, index) : time
}


const floorTime = (timestr) => {

  if (!timestr) {
    return 0
  }

  return timestr.split(":").slice(0, 2).join(":")
}


// round to nearest number dividable by mod
const roundMod = (number, mod) => {
    return Math.ceil( Math.floor(number) / mod) * mod
}


const getBrowserLocales = (options = {}) => {
  const defaultOptions = {
    languageCodeOnly: false,
  };

  const opt = {
    ...defaultOptions,
    ...options,
  };

  const browserLocales =
    navigator.languages === undefined
      ? [navigator.language]
      : navigator.languages;

  if (!browserLocales) {
    return undefined;
  }

  return browserLocales.map(locale => {
    const trimmedLocale = locale.trim();

    return opt.languageCodeOnly
      ? trimmedLocale.split(/-|_/)[0]
      : trimmedLocale;
  });
}

/*
round to nearest 15min

let hhmm = timestr.split(":").slice(0, 2);
let hours = Number(hhmm[0]);
let minutes = Number(hhmm[1]);

let m = (((minutes + 7.5)/15 | 0) * 15) % 60;
let h = ((((minutes/105) + .5) | 0) + hours) % 24;
return String(hours).padStart(2, "0") + ":" + String(minutes).padStart(2, "0");
*/

export { getToday, getTime, floorTime, roundMod, getBrowserLocales }
