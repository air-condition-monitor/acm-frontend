import React from 'react'
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'


// TODO: proper colors, position and shape
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
}))


const DeviceMenu = ({entries, chosen, setChosen}) => {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [selectedIndex, setSelectedIndex] = React.useState(1)

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setAnchorEl(null)
    setChosen(entries[index])
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div className={classes.root}>

      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        {!chosen ? "Choose a device" : chosen}
      </Button>

      <Menu
        id="lock-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {entries.map((option, index) => (
          <MenuItem
            key={option}
            selected={index === selectedIndex}
            onClick={(event) => handleMenuItemClick(event, index)}
          >
            {option}
          </MenuItem>
        ))
        }
      </Menu>
    </div>
  )
}

const DevicePicker = ({devices, chosen, setChosen}) => {

  return (
    <div>
      <DeviceMenu entries={devices} chosen={chosen} setChosen={setChosen}/>
    </div>
  )


}

export default DevicePicker
