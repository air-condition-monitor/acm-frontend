FROM node:16-alpine
WORKDIR /frontend
ENV PATH /frontend/node_modules/.bin:$PATH

COPY package.json package-lock.json ./
COPY ./src ./src
COPY ./public ./public

RUN npm install
RUN npm run build
